﻿# NightDash

NightDash is an easy-to-use application, that assists Victorian Depressed Youngsters (aged 18 - 24) for Night Running. 

Night running helps to get better sleep and relaxed days.

## Contents

* [NightDash][]: Android app to assits night runners with parks and routes to run on, and food to take before their run.
* [NightDash - Website][]: Web application for educating about overcoming depression and how night running helps to overcome it.
* [NightDash - AWS Cloud Lambda Functions][]: Functions used to fetch data from database and send to app via API Gateway.
* [NightDash - Power BI][]: Park visualization designed using power BI
* [NightDash - Data Wrangling][]: Source code for wrnalging all datasets.

[NightDash]: https://gitlab.com/Vijnathi/nightdash/tree/master/NightDash
[NightDash - Website]: https://gitlab.com/Vijnathi/nightdash/tree/master/NightDash%20-%20Website
[NightDash - AWS Cloud Lambda Functions]: https://gitlab.com/Vijnathi/nightdash/tree/master/NightDash%20-%20AWS%20Cloud%20Lambda%20Function
[NightDash - Power BI]: https://gitlab.com/Vijnathi/nightdash/tree/master/NightDash%20-%20Power%20BI
[NightDash - Data Wrangling]: https://gitlab.com/Vijnathi/nightdash/tree/master/NightDash%20-%20Data%20Wrangling

## Contributors

**Team E14 - Divide And Conquer**

**Topic** - Promoting health through physical activities and sports
**Project** - NightDash

**Team Members -** 

* **Vijnathi Katamaneni** - Master of Information Technology - Application Developer
* **Vivardhan Ramesh** - Master of Information Technology - Applicaiton Developer
* **Yiyuan Jiang (Barton)** - Master of Data Science - Data Analyst
* **Shiyang Sun (Steven)** - Master of Business Information System - Business Analyst
