﻿# Web App

* Educates Victorian youngsters how to overcome depression and how night running helps to reduce depression.
* Promotes android app

## Contributor

**Team E14 - Divide And Conquer**

**Topic** - Promoting health through physical activities and sports
**Project** - NightDash

**Team Members -** 
* **Vijnathi Katamaneni** - Master of Information Technology - Application Developer
* **Vivardhan Ramesh** - Master of Information Technology - Application Developer
