﻿# AWS Lambda Functions used for app

Upload these function in lambda services as follows:
* Open the [AWS Lambda console][].
* Choose Functions on the navigation pane, and then open your function.
* In the Function code section, expand the Code entry type drop-down list, and then choose Upload a .ZIP file.
* Choose Upload, and then select your .zip file.
* Choose Save.

[AWS Lambda console]: https://aws.amazon.com/lambda/

## Contributor

**Team E14 - Divide And Conquer**

**Topic** - Promoting health through physical activities and sports
**Project** - NightDash

**Team Members -** 
* **Vijnathi Katamaneni** - Master of Information Technology - Application Developer
* **Vivardhan Ramesh** - Master of Information Technology - Application Developer
