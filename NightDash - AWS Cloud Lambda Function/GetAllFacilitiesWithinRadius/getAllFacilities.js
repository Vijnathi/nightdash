var sql = require("mysql"); 

exports.handler = function(event, context, callback) {
    
    var source_latitude = event.latitude;
    var source_longitude = event.longitude;
    var radius = event.radius;
    var surface_type = event.surface_type;
    
    var surface_types;
    var facilities;
    
    console.log(source_latitude);
    console.log(source_longitude);
    console.log(radius);
    
    var conn=sql.createConnection({
		host:'database-nightdash.cyegr4lrlwvx.ap-southeast-2.rds.amazonaws.com',
		user:'NightDashAdmin',
		password:'nightdashe14',
		database:'NightDash',
		port: 3306,
		connectTimeout: 100000
	});

	// getSurfaces();
	
	function getSurfaces() {
		conn.connect();
	    conn.query('SELECT * FROM NightDash.Surfaces WHERE UPPER(Surface_Type) LIKE  UPPER(?)',["%"+surface_type+"%"], function(err,rows,fields){
		
			if (err) {
				context.done(
					null,
					{
					"status":"error",
					"error":JSON.stringify(err, null, 2)
					}
				);
			}
			else {
				// context.done(
				// 	null,
				// 	{
				// 	"status":"success",
				// 	"data": rows
				// 	}
				// );
				surface_types = rows;
				
			    console.log(rows);
			    
			}
		});
	    conn.end();
	}
	
	// function getParks() {
	// 	conn.connect();
	// 	for()
	//     conn.query('SELECT * FROM Parks WHERE Surface_ID = ?',[], function(err,rows,fields){
		
	// 		if (err) {
	// 			context.done(
	// 				null,
	// 				{
	// 				"status":"error",
	// 				"error":JSON.stringify(err, null, 2)
	// 				}
	// 			);
	// 		}
	// 		else {
	// 			// context.done(
	// 			// 	null,
	// 			// 	{
	// 			// 	"status":"success",
	// 			// 	"data": rows
	// 			// 	}
	// 			// );
	// 			facilities = rows;
				
	// 		    console.log(rows);
			    
	// 		}
	// 	});
	//     conn.end();
	// }
	
	getFacilities();
	function getFacilities() {
	    conn.connect();
	    conn.query('SELECT *,(((acos(sin((?*pi()/180)) * sin((dest.latitude*pi()/180))+cos((?*pi()/180))*cos((dest.latitude*pi()/180))*cos(((?-dest.longitude)*pi()/180))))*180/pi())*60*1.1515*1609.344) as distance FROM Facilities AS dest HAVING distance < ? ORDER BY distance ASC',[source_latitude, source_latitude, source_longitude, radius], function(err,rows,fields){
		
			if (err) {
				context.done(
					null,
					{
					"status":"error",
					"error":JSON.stringify(err, null, 2)
					}
				);
			}
			else {
				context.done(
					null,
					{
					"status":"success",
					"data": rows
					}
				);
				facilities = rows;
				
			    console.log(rows);
			    
			}
		});
	    conn.end();
	}
	
	

};
