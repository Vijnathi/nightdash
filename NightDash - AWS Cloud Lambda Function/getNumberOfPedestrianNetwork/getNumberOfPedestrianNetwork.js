var sql = require("mysql"); 
var aws = require("aws-sdk");
const s3 = new aws.S3();

exports.handler = function(event, context, callback) {
    
  var latitude = event.latitude;
  var longitude = event.longitude;
  var radius = event.radius;

  var data;

  async function readFile(Bucket, Key) {
    const params = {
      Bucket,
      Key,
      ResponseContentType: 'application/json',
    };

    const f = await s3.getObject(params).promise();
    return f.Body.toString('utf-8');
  }

  readFile('pedestrian-network', 'Pedestrian_network.json').then(response => getData(response));

  function getData(response) {
    // console.log(JSON.parse(response))
    var data = JSON.parse(response)

    var features = data.features;

    
    var selected_features = [];

    for (feature in features) {

        if(features[feature].properties.DESCRIPTION == "Pestrian Footpath") {
          var coordinate = features[feature].geometry.coordinates[0];
          var dest_latitude = coordinate[1]
          var dest_longitude = coordinate[0]
          var distance = calculateDistance(latitude, longitude, dest_latitude, dest_longitude);

          if (distance < radius) {
              var selected_feature = {
                "type" : "Feature",
                "geometry" : {
                  "type" : "LineString",
                  "coordinates" : features[feature].geometry.coordinates
                },
                "properties" : {
                  "length": features[feature].properties.Shape_Length,
                  "distance": distance
                }
              }
              selected_features.push(selected_feature); 
                
          }
        }
    }
    // console.log(selected_features);
    var selected_features_sorted = selected_features.sort(function(a, b) {
        return parseFloat(a.properties.distance) - parseFloat(b.properties.distance);
    });
    // console.log(selected_features_sorted);

    // var coordinates_final = []
    // for (index in selected_features_sorted) {
    //     var coordinates = selected_features_sorted[index].coordinates

    //     for (coordinate in coordinates) {
    //         coordinates_final.push(coordinates[coordinate])
    //     }
    // }

    context.done(
      null,
      {
        "status": "success",
        "data": [{"Count":selected_features_sorted.length}]
      }
    );
    // console.log(coordinates_final);
  }

  function calculateDistance(src_latitude, src_longitude, dest_latitude, dest_longitude) {
      if ((src_latitude == dest_latitude) && (src_longitude == dest_longitude)) {
          return 0;
      }
      else {
          var radlat1 = Math.PI * src_latitude / 180;
          var radlat2 = Math.PI * dest_latitude / 180;
          var theta = src_longitude - dest_longitude;
          var radtheta = Math.PI * theta / 180;
          var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
          
          if (dist > 1) {
              dist = 1;
          }
          
          dist = Math.acos(dist);
          dist = dist * 180 / Math.PI;
          dist = dist * 60 * 1.1515;
          dist = dist * 1.609344
        
          return dist * 1000;
      }
  }

};
