var sql = require("mysql"); 

exports.handler = function(event, context, callback) {
    
    var source_latitude = event.latitude;
    var source_longitude = event.longitude;
    var radius = event.radius;
    
    var conn=sql.createConnection({
		host:'database-nightdash.cyegr4lrlwvx.ap-southeast-2.rds.amazonaws.com',
		user:'NightDashAdmin',
		password:'nightdashe14',
		database:'NightDash',
		port: 3306,
		connectTimeout: 100000
	});

	conn.connect();
	conn.query('select LONGITUDE, LATITUDE, ACCIDENT_TIME, TOTAL_PERSONS, PEDESTRIAN, MALES, FEMALES FROM Car_crash_version_one WHERE (((acos(sin((?*pi()/180)) * sin((LATITUDE*pi()/180))+cos((?*pi()/180))*cos((LATITUDE*pi()/180))*cos(((?-LONGITUDE)*pi()/180))))*180/pi())*60*1.1515*1609.344) < ?;', [source_latitude, source_latitude, source_longitude, radius], function(err,rows,fields){
		
		if (err) {
			context.done(
				null,
				{
					"status":"error",
					"error":JSON.stringify(err, null, 2)
				}
			);
		} else {
			context.done(
				null,
				{
					"status":"success",
					"data": rows
				}
			);
		}
	});
	
	conn.end();
	
};
