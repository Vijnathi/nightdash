var sql = require("mysql"); 

exports.handler = function(event, context, callback) {
    
    var conn=sql.createConnection({
		host:'database-nightdash.cyegr4lrlwvx.ap-southeast-2.rds.amazonaws.com',
		user:'NightDashAdmin',
		password:'nightdashe14',
		database:'NightDash',
		port: 3306,
		connectTimeout: 100000
	});

    conn.connect();
    conn.query('SELECT * FROM Food', function(err,rows,fields){
	
		if (err) {
			context.done(
				null,
				{
				"status":"error",
				"error":JSON.stringify(err, null, 2)
				}
			);
		}
		else {
			var twoHours = []
			var fourHours = []
			var sixHours = []
			var eightHours = []
			for (var i = 0; i < rows.length; i++) {
				var caloriesWithDietaryFibre = rows[i]["Energy, with dietary fibre (kJ)"] / 4.184;
				// var caloriesWithoutDietaryFibre = rows[i]["Energy, without dietary fibre (kJ)"] / 4.184;
				var foodName = rows[i]["Food Name"];
				var item = {
						"Calories": caloriesWithDietaryFibre,
						"FoodName": foodName
					}
				if(caloriesWithDietaryFibre <= 300.0) {
					if(foodName.toLowerCase().includes("fruit") || caloriesWithDietaryFibre <= 100.0) {
						sixHours.push(item)
					} else {
						if(caloriesWithDietaryFibre >= 200.0 && caloriesWithDietaryFibre <= 300.0){
							twoHours.push(item);
						}
					}
				}
				if(caloriesWithDietaryFibre > 450.0 && caloriesWithDietaryFibre <= 550.0) {
					fourHours.push(item);
				}
				if(caloriesWithDietaryFibre > 600.0 && caloriesWithDietaryFibre <= 900.0) {
					eightHours.push(item);
				}
				// if(caloriesWithDietaryFibre > 900.0 && caloriesWithDietaryFibre <= 1200.0) {
				// 	eightHours.push(item);
					
				// }
				// if(caloriesWithoutDietaryFibre <= 300.0) {
					
				// }
				// if(caloriesWithoutDietaryFibre > 300.0 && caloriesWithoutDietaryFibre <= 600.0) {
					
				// }
				// if(caloriesWithoutDietaryFibre > 600.0 && caloriesWithoutDietaryFibre <= 900.0) {
					
				// }
				// if(caloriesWithoutDietaryFibre > 900.0 && caloriesWithoutDietaryFibre <= 1200.0) {
					
				// }
			}
			console.log(twoHours.length)
			console.log(fourHours.length)
			console.log(sixHours.length)
			console.log(eightHours.length)
			context.done(
				null,
				{
				"status":"success",
				"data": [{
					"twoHours": twoHours,
					"fourHours": fourHours,
					"sixHours": sixHours,
					"eightHours": eightHours
				}]
				}
			);
		}
	});
    conn.end();

};
