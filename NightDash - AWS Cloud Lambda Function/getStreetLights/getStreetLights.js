var sql = require("mysql"); 

exports.handler = function(event, context, callback) {
    
    var radius = event.radius;
    var source_latitude = event.latitude;
    var source_longitude = event.longitude;
    
    var conn=sql.createConnection({
		host:'database-nightdash.cyegr4lrlwvx.ap-southeast-2.rds.amazonaws.com',
		user:'NightDashAdmin',
		password:'nightdashe14',
		database:'NightDash',
		port: 3306,
		connectTimeout: 100000
	});

    conn.connect();
    conn.query('select lon as LONGITUDE, lat as LATITUDE FROM StreetLights WHERE (((acos(sin((?*pi()/180)) * sin((lat*pi()/180))+cos((?*pi()/180))*cos((lat*pi()/180))*cos(((?-lon)*pi()/180))))*180/pi())*60*1.1515*1609.344) < ?;', [source_latitude, source_latitude, source_longitude, radius], function(err,rows,fields){
	
		if (err) {
			context.done(
				null,
				{
				"status":"error",
				"error":JSON.stringify(err, null, 2)
				}
			);
		}
		else {
			context.done(
				null,
				{
				"status":"success",
				"data": rows
				}
			);
		}
	});
    conn.end();

};
