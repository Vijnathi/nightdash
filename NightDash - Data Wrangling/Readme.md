﻿# Data Wrangling of all Datasets

Datasets used in this application can be wrangled using this file.

To run this file you need to [install Jupyter Notebook][]

[install Jupyter Notebook]: https://jupyter.org/

## Contributor

**Team E14 - Divide And Conquer**

**Topic** - Promoting health through physical activities and sports
**Project** - NightDash

**Team Member -** 
* **Yiyuan Jiang (Barton)** - Master of Data Science - Data Analyst
