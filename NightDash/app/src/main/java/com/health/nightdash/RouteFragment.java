package com.health.nightdash;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mapbox.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.core.exceptions.ServicesException;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class RouteFragment  extends Fragment implements OnMapReadyCallback, MapboxMap.OnMapClickListener{
    View vRoute;

    private MapView mapView;
    private MapboxMap mapboxMap;

    private JSONObject geoJSONData;

    private FrameLayout routeMapLayout;
    private LinearLayout statsLayout;

    private TextView totalCrimeTextView;

    FrameLayout progressBarHolder;
    AlphaAnimation inAnimation;
    AlphaAnimation outAnimation;

    private LinearLayout routesInfoLayout, crimesLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((HomeActivity)getActivity()).showStatusBar();
        Mapbox.getInstance(getActivity().getApplicationContext(), getString(R.string.access_token));

        vRoute = inflater.inflate(R.layout.fragment_route, container, false);
        ((HomeActivity)getActivity()).disableDisplayHome();
        routeMapLayout = (FrameLayout) vRoute.findViewById(R.id.routeMapLayout);
        statsLayout = (LinearLayout) vRoute.findViewById(R.id.statsLayout);

        totalCrimeTextView = (TextView) vRoute.findViewById(R.id.totalCrime);

        statsLayout.setVisibility(View.VISIBLE);
        mapView = vRoute.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        routesInfoLayout = (LinearLayout) vRoute.findViewById(R.id.routeInfo);
        crimesLayout = (LinearLayout) vRoute.findViewById(R.id.crimesLayout);
        routesInfoLayout.setVisibility(View.VISIBLE);
        crimesLayout.setVisibility(View.GONE);
        progressBarHolder = (FrameLayout) vRoute.findViewById(R.id.progressBarHolder);
        progressBarHolder.setClickable(false);

        return vRoute;
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {

            @Override
            public void onStyleLoaded(@NonNull Style style) {

                mapboxMap.addOnMapClickListener(RouteFragment.this);
                addDestinationIconSymbolLayer(style);
            }
        });
    }

    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {

        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        loadedMapStyle.addSource(geoJsonSource);
        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");
        destinationSymbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
        );
        loadedMapStyle.addLayer(destinationSymbolLayer);
    }

    @SuppressWarnings({"MissingPermission"})
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        Point userClickedPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());
        System.out.println(userClickedPoint.latitude() + " " + userClickedPoint.longitude());
        GeoJsonSource source = mapboxMap.getStyle().getSourceAs("destination-source-id");
        if (source != null) {
            source.setGeoJson(Feature.fromGeometry(userClickedPoint));
        }

        mapboxMap.setCameraPosition(new CameraPosition.Builder()
                .target(point)
                .zoom(15)
                .build());

        getPostcode(userClickedPoint);
        Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.content_frame);
        if (currentFragment instanceof RouteFragment) {
            new LoadGeoJson((RouteFragment) currentFragment, userClickedPoint).execute();
        }
        new GetCrashes().execute(point);
        return true;

    }

    // resource: https://docs.mapbox.com/android/maps/examples/location-picker/
    private void getPostcode(Point point) {

        try {
            MapboxGeocoding client = MapboxGeocoding.builder()
                    .accessToken(getString(R.string.access_token))
                    .query(Point.fromLngLat(point.longitude(), point.latitude()))
                    .geocodingTypes(GeocodingCriteria.TYPE_POSTCODE)
                    .build();

            client.enqueueCall(new Callback<GeocodingResponse>() {

                @Override
                public void onResponse(Call<GeocodingResponse> call, Response<GeocodingResponse> response) {

                    if (response.body() != null) {
                        List<CarmenFeature> results = response.body().features();
                        if (results.size() > 0) {
                            CarmenFeature feature = results.get(0);

                            String place = feature.placeName();

                            String[] postcodeStr = place.split(",");
                            new GetTotalCrimes().execute(Integer.parseInt(postcodeStr[0]));

                        }
                    }
                }

                @Override
                public void onFailure(Call<GeocodingResponse> call, Throwable throwable) {
                    Timber.e("Geocoding Failure: %s", throwable.getMessage());
                }
            });
        } catch (ServicesException servicesException) {
            Timber.e("Error geocoding: %s", servicesException.toString());
            servicesException.printStackTrace();
        }
    }
    private void drawLines(@NonNull FeatureCollection featureCollection) {
        if (mapboxMap != null) {
            mapboxMap.getStyle(style -> {
                System.out.println(featureCollection);
                if (featureCollection.features() != null) {
                    if (featureCollection.features().size() > 0) {

                        style.addSource(new GeoJsonSource("line-source", featureCollection));

                        // The layer properties for our line. This is where we make the line dotted, set the
                        // color, etc.
                        style.addLayer(new LineLayer("linelayer", "line-source")
                                .withProperties(PropertyFactory.lineCap(Property.LINE_CAP_SQUARE),
                                        PropertyFactory.lineJoin(Property.LINE_JOIN_MITER),
                                        PropertyFactory.lineOpacity(.7f),
                                        PropertyFactory.lineWidth(7f),
                                        PropertyFactory.lineColor(getResources().getColor(R.color.colorPrimaryDark))));
//                                        PropertyFactory.lineColor(Color.parseColor("#3bb2d0"))));
                        ((HomeActivity)getActivity()).enableDisplayHome();
                        outAnimation = new AlphaAnimation(1f, 0f);
                        outAnimation.setDuration(200);
                        progressBarHolder.setAnimation(outAnimation);
                        progressBarHolder.setVisibility(View.GONE);
                    } else {
                        ((HomeActivity)getActivity()).enableDisplayHome();
                        outAnimation = new AlphaAnimation(1f, 0f);
                        outAnimation.setDuration(200);
                        progressBarHolder.setAnimation(outAnimation);
                        progressBarHolder.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage("Paths are not available at this location, only crime and crash details are displayed");
                        alertDialogBuilder.setPositiveButton("Done",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {

                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                    statsLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private class LoadGeoJson extends AsyncTask<Void, Void, FeatureCollection> {

        private WeakReference<RouteFragment> weakReference;
        private Point point;

        LoadGeoJson(RouteFragment fragment, Point point) {
            this.weakReference = new WeakReference<RouteFragment>(fragment);
            this.point = point;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.setDuration(200);
            progressBarHolder.setAnimation(inAnimation);
            progressBarHolder.setVisibility(View.VISIBLE);
        }

        @Override
        protected FeatureCollection doInBackground(Void... voids) {
            try {
                RouteFragment activity = weakReference.get();
                if (activity != null) {
                    SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                    String details = preferences.getString("details", null);
                    String[] detail = details.split(" ");

                    geoJSONData = new JSONObject();
                    try {
                        geoJSONData.put("type", "FeatureCollection");
                        String paths = RestClient.findAllPaths(250, point.latitude(), point.longitude());

                        JSONObject pathsObject = new JSONObject(paths);
                        JSONArray features = pathsObject.getJSONArray("data");
                        geoJSONData.put("features", features);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return FeatureCollection.fromJson(geoJSONData.toString());
                }
            } catch (Exception exception) {
                Timber.e("Exception Loading GeoJSON: %s" , exception.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(@Nullable FeatureCollection featureCollection) {
            super.onPostExecute(featureCollection);
            RouteFragment activity = weakReference.get();
            mapboxMap.getStyle(style -> {
                List<Source> sources = style.getSources();
                List<Layer> layers = style.getLayers();
                if(sources.size() > 3) {
                    style.removeLayer("linelayer");
                    style.removeSource("line-source");
                }
            });
            if (activity != null && featureCollection != null) {
                activity.drawLines(featureCollection);
            }
        }
    }

    private class GetTotalCrimes extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {
            return RestClient.getCrimeStats(params[0]);
        }

        @Override
        protected void onPostExecute(String crimes) {
            routesInfoLayout.setVisibility(View.GONE);
            crimesLayout.setVisibility(View.VISIBLE);
            try {
                JSONObject object = new JSONObject(crimes);
                JSONArray array = object.getJSONArray("data");
                JSONObject data = array.getJSONObject(0);
                totalCrimeTextView.setText(data.getString("Total Incidents by Postcode") + " Crimes");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetCrashes extends AsyncTask<LatLng, Void, String> {
        @Override
        protected String doInBackground(LatLng... params) {
            return RouteRestClient.getCrashes(250, params[0].getLatitude(), params[0].getLongitude());
        }

        @Override
        protected void onPostExecute(String crashes) {
            IconFactory iconFactory = IconFactory.getInstance(getActivity());
            Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.crash_icon);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Icon icon = iconFactory.fromBitmap(bitmap);

            mapboxMap.clear();

            try {
                JSONObject jsonObject = new JSONObject(crashes);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                List<Feature> markerCoordinates = new ArrayList<>();
                MarkerOptions markerOptions = new MarkerOptions();
                Boolean plotting = true;
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        if (i == jsonArray.length() - 1) {
                            plotting = false;
                        }
                        JSONObject crashObject = jsonArray.getJSONObject(i);

                        markerOptions.position(new LatLng(crashObject.getDouble("LATITUDE"), crashObject.getDouble("LONGITUDE")));
                        markerOptions.setIcon(icon);
                        mapboxMap.addMarker(markerOptions);
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetStreetLights extends AsyncTask<LatLng, Void, String> {
        @Override
        protected String doInBackground(LatLng... params) {
            return RouteRestClient.getStreetLights(250, params[0].getLatitude(), params[0].getLongitude());
        }

        @Override
        protected void onPostExecute(String streetLights) {
            IconFactory iconFactory = IconFactory.getInstance(getActivity());
            Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.street_light_icon);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Icon icon = iconFactory.fromBitmap(bitmap);


            try {
                JSONObject jsonObject = new JSONObject(streetLights);
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                List<Feature> markerCoordinates = new ArrayList<>();
                MarkerOptions markerOptions = new MarkerOptions();
                Boolean plotting = true;
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {

                        if (i == jsonArray.length() - 1) {
                            plotting = false;
                        }
                        JSONObject streetLightsObject = jsonArray.getJSONObject(i);

                        markerOptions.position(new LatLng(streetLightsObject.getDouble("LATITUDE"), streetLightsObject.getDouble("LONGITUDE")));
                        markerOptions.setIcon(icon);
                        mapboxMap.addMarker(markerOptions);
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

}
