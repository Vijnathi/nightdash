package com.health.nightdash;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {UserDetails.class}, version = 2, exportSchema = false)

public abstract class UserDetailsDatabase extends RoomDatabase {
    public abstract UserDetailsDAO userDetailsDAO();

    private static volatile UserDetailsDatabase INSTANCE;
    static UserDetailsDatabase getDatabase(final Context context)
    {
        if (INSTANCE == null)
        {
            synchronized (UserDetailsDatabase.class)
            {
                if (INSTANCE == null)
                {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(), UserDetailsDatabase.class,"UserDetailsDatabase").build();
                }
            }
        }
        return INSTANCE;
    }
}
