package com.health.nightdash;

public class FoodDataModel {
    private String title = "";
    private Double Calories = 0.0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getCalories() {
        return Calories;
    }

    public void setCalories(Double calories) {
        Calories = calories;
    }
}
