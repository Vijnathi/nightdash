package com.health.nightdash;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CustomFoodCardAdapter extends RecyclerView.Adapter<CustomFoodCardAdapter.MyViewHolder> implements Filterable {

    ArrayList<FoodDataModel> listDataModel = new ArrayList<FoodDataModel>();
    ArrayList<FoodDataModel> filteredListDataModel = new ArrayList<FoodDataModel>();

    FoodFilter foodFilter;

    public CustomFoodCardAdapter(ArrayList<FoodDataModel> _listDataModel) {
        this.listDataModel = _listDataModel;
        this.filteredListDataModel = _listDataModel;

    }

    @Override
    public CustomFoodCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardinterface, parent, false);
        CustomFoodCardAdapter.MyViewHolder holder = new CustomFoodCardAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomFoodCardAdapter.MyViewHolder holder, int position) {

        FoodDataModel objDataModel = listDataModel.get(position);

        holder.textViewFood.setText(objDataModel.getTitle().toString());
        holder.textViewCalories.setText(String.format ("%.2f", objDataModel.getCalories()) + " KCal");
    }

    @Override
    public Filter getFilter() {
        if(foodFilter == null)
            foodFilter = new FoodFilter(this, listDataModel);
        return foodFilter;
    }

    @Override
    public int getItemCount() {
        return listDataModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewFood, textViewCalories;
        EditText searchEditText;

        public MyViewHolder(View view) {
            super(view);
            textViewFood = (TextView) view.findViewById(R.id.foodTitle);
            textViewCalories = (TextView) view.findViewById(R.id.calories);
            searchEditText = (EditText) view.findViewById(R.id.searchBox);

        }
    }

    private static class FoodFilter extends Filter {

        private final CustomFoodCardAdapter adapter;

        private final List<FoodDataModel> originalList;

        private final List<FoodDataModel> filteredList;

        private FoodFilter(CustomFoodCardAdapter adapter, List<FoodDataModel> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final FoodDataModel foodDataModel : originalList) {
                    if (foodDataModel.getTitle().contains(filterPattern)) {
                        filteredList.add(foodDataModel);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredListDataModel.clear();
            adapter.filteredListDataModel.addAll((ArrayList<FoodDataModel>) results.values);
            adapter.notifyDataSetChanged();
        }
    }
}
