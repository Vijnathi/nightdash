package com.health.nightdash;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDetailsDAO {
    @Query("SELECT * FROM UserDetails LIMIT 1")
    List<UserDetails> getAll();
    @Query("SELECT * FROM UserDetails WHERE userDetailsid = :userDetailsid LIMIT 1")
    UserDetails findByID(int userDetailsid);
    @Insert
    void insertAll(UserDetails... userDetails);
    @Insert
    long insert(UserDetails userDetails);
    @Delete
    void delete(UserDetails userDetails);
    @Update(onConflict = REPLACE)
    public void updateUsers(UserDetails... userDetails);
    @Query("DELETE FROM UserDetails")
    void deleteAll();

}
