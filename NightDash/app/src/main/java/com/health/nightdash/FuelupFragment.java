package com.health.nightdash;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FuelupFragment extends Fragment {
    View vFuelUp;

    private RecyclerView recyclerView;
    private RecyclerView recyclerViewFour;
    private RecyclerView recyclerViewSix;
    private RecyclerView recyclerViewEight;

    private EditText searchEditText;

    FrameLayout progressBarHolder;
    AlphaAnimation inAnimation;
    AlphaAnimation outAnimation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((HomeActivity)getActivity()).showStatusBar();
        vFuelUp = inflater.inflate(R.layout.fragment_fuelup, container, false);


        ((HomeActivity)getActivity()).enableBottomBar(false);
        ((HomeActivity)getActivity()).disableDisplayHome();
        progressBarHolder = (FrameLayout) vFuelUp.findViewById(R.id.progressBarHolder);
        progressBarHolder.setClickable(false);
        inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        progressBarHolder.setAnimation(inAnimation);
        progressBarHolder.setVisibility(View.VISIBLE);

        new GetFood().execute();

        searchEditText = (EditText) vFuelUp.findViewById(R.id.searchBox);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                String foods = preferences.getString("foods", null);
                plotRecyclerView(foods, "search");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return vFuelUp;
    }

    private class GetFood extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            return FuelUpRestClient.getFood();
        }

        @Override
        protected void onPostExecute(String foods) {
            SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("foods", foods);
            editor.apply();
            plotRecyclerView(foods, "main");
            ((HomeActivity)getActivity()).enableBottomBar(true);
            ((HomeActivity)getActivity()).enableDisplayHome();
            outAnimation = new AlphaAnimation(1f, 0f);
            outAnimation.setDuration(200);
            progressBarHolder.setAnimation(outAnimation);
            progressBarHolder.setVisibility(View.GONE);
        }
    }

    public void plotRecyclerView(String foods, String from) {

        ArrayList<FoodDataModel> objListTwoHours = new ArrayList<FoodDataModel>();
        ArrayList<FoodDataModel> objListFourHours = new ArrayList<FoodDataModel>();
        ArrayList<FoodDataModel> objListSixHours = new ArrayList<FoodDataModel>();
        ArrayList<FoodDataModel> objListEightHours = new ArrayList<FoodDataModel>();
        FoodDataModel fdm;
        try {
            JSONObject object = new JSONObject(foods);
            JSONArray array = object.getJSONArray("data");
            JSONArray twoHoursArray = array.getJSONObject(0).getJSONArray("twoHours");
            JSONArray fourHoursArray = array.getJSONObject(0).getJSONArray("fourHours");
            JSONArray sixHoursArray = array.getJSONObject(0).getJSONArray("sixHours");
            JSONArray eightHoursArray = array.getJSONObject(0).getJSONArray("eightHours");

            for (int i = 0; i < twoHoursArray.length(); i++) {
                if(twoHoursArray.getJSONObject(i).getDouble("Calories") > 0.0) {
                    fdm = new FoodDataModel();
                    fdm.setTitle(twoHoursArray.getJSONObject(i).getString("FoodName"));
                    fdm.setCalories(twoHoursArray.getJSONObject(i).getDouble("Calories"));
                    objListTwoHours.add(fdm);
                }
            }

            for (int i = 0; i < fourHoursArray.length(); i++) {
                if(fourHoursArray.getJSONObject(i).getDouble("Calories") > 0.0) {
                    fdm = new FoodDataModel();
                    fdm.setTitle(fourHoursArray.getJSONObject(i).getString("FoodName"));
                    fdm.setCalories(fourHoursArray.getJSONObject(i).getDouble("Calories"));
                    objListFourHours.add(fdm);
                }
            }

            for (int i = 0; i < sixHoursArray.length(); i++) {
                if(sixHoursArray.getJSONObject(i).getDouble("Calories") > 0.0) {
                    fdm = new FoodDataModel();
                    fdm.setTitle(sixHoursArray.getJSONObject(i).getString("FoodName"));
                    fdm.setCalories(sixHoursArray.getJSONObject(i).getDouble("Calories"));
                    objListSixHours.add(fdm);
                }
            }

            for (int i = 0; i < eightHoursArray.length(); i++) {
                if(eightHoursArray.getJSONObject(i).getDouble("Calories") > 0.0) {
                    fdm = new FoodDataModel();
                    fdm.setTitle(eightHoursArray.getJSONObject(i).getString("FoodName"));
                    fdm.setCalories(eightHoursArray.getJSONObject(i).getDouble("Calories"));
                    objListEightHours.add(fdm);
                }
            }

            ArrayList<FoodDataModel> _listDataModel = objListTwoHours;
            ArrayList<FoodDataModel> _listDataModelFour = objListFourHours;
            ArrayList<FoodDataModel> _listDataModelSix = objListSixHours;
            ArrayList<FoodDataModel> _listDataModelEight = objListEightHours;


            recyclerView = (RecyclerView) vFuelUp.findViewById(R.id.recyclerView);
            recyclerViewFour = (RecyclerView) vFuelUp.findViewById(R.id.recyclerViewFour);
            recyclerViewSix = (RecyclerView) vFuelUp.findViewById(R.id.recyclerViewSix);
            recyclerViewEight = (RecyclerView) vFuelUp.findViewById(R.id.recyclerViewEight);

            recyclerView.setHasFixedSize(true);
            recyclerViewFour.setHasFixedSize(true);
            recyclerViewSix.setHasFixedSize(true);
            recyclerViewEight.setHasFixedSize(true);

                /* Set Horizontal LinearLayout to RecyclerView */
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            LinearLayoutManager mLayoutManagerFour = new LinearLayoutManager(getActivity().getApplicationContext());
            mLayoutManagerFour.setOrientation(LinearLayoutManager.HORIZONTAL);
            LinearLayoutManager mLayoutManagerSix = new LinearLayoutManager(getActivity().getApplicationContext());
            mLayoutManagerSix.setOrientation(LinearLayoutManager.HORIZONTAL);
            LinearLayoutManager mLayoutManagerEight = new LinearLayoutManager(getActivity().getApplicationContext());
            mLayoutManagerEight.setOrientation(LinearLayoutManager.HORIZONTAL);

            recyclerView.setLayoutManager(mLayoutManager);
            recyclerViewFour.setLayoutManager(mLayoutManagerFour);
            recyclerViewSix.setLayoutManager(mLayoutManagerSix);
            recyclerViewEight.setLayoutManager(mLayoutManagerEight);

            CustomFoodCardAdapter adapter = new CustomFoodCardAdapter(_listDataModel);
            CustomFoodCardAdapter adapterFour = new CustomFoodCardAdapter(_listDataModelFour);
            CustomFoodCardAdapter adapterSix = new CustomFoodCardAdapter(_listDataModelSix);
            CustomFoodCardAdapter adapterEight = new CustomFoodCardAdapter(_listDataModelEight);

            if (from.equalsIgnoreCase("search")) {
                adapter.getFilter().filter(searchEditText.getText().toString());
                recyclerView.setAdapter(adapter);
                adapterFour.getFilter().filter(searchEditText.getText().toString());
                recyclerViewFour.setAdapter(adapterFour);
                adapterSix.getFilter().filter(searchEditText.getText().toString());
                recyclerViewSix.setAdapter(adapterSix);
                adapterEight.getFilter().filter(searchEditText.getText().toString());
                recyclerViewEight.setAdapter(adapterEight);
            } else {
                recyclerView.setAdapter(adapter);
                recyclerViewFour.setAdapter(adapterFour);
                recyclerViewSix.setAdapter(adapterSix);
                recyclerViewEight.setAdapter(adapterEight);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
