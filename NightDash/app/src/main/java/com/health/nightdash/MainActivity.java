package com.health.nightdash;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager sliderViewPager;
    private LinearLayout dotLayout;

    private TextView[] dots;

    private SliderAdapter sliderAdapter;

    private Button startButton;

    private int currentPage;

    private UserDetailsDatabase db = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ActionBar myActionBar = getSupportActionBar();
        myActionBar.hide();

        db = Room.databaseBuilder(getApplicationContext(), UserDetailsDatabase.class, "UserDetailsDatabase").fallbackToDestructiveMigration().build();


        sliderViewPager = (ViewPager) findViewById(R.id.slideViewPager);
        dotLayout = (LinearLayout) findViewById(R.id.dotsPanel);

        startButton = (Button) findViewById(R.id.startButton);

        ReadDatabase readDatabase = new ReadDatabase();
        readDatabase.execute();

    }


    private class ReadDatabase extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            List<UserDetails> userDetails = db.userDetailsDAO().getAll();
            if (!(userDetails.isEmpty() || userDetails == null)) {
                String allUsers = "";
                for (UserDetails temp : userDetails) {
                    String userstr = (temp.getUserDetailsid() + " " + temp.getRadius() + " " + temp.getDistance() + " " + temp.getTime() + " " + temp.getSurface_type() + " " + temp.getFirst_time());
                    allUsers = allUsers + userstr;
                }
                return allUsers;
            } else return "";
        }

        @Override
        protected void onPostExecute(String details) {
            if(details.equals("")){
                sliderAdapter = new SliderAdapter(getApplicationContext());
                sliderViewPager.setAdapter(sliderAdapter);

                addDotsIndicator(0);
                sliderViewPager.addOnPageChangeListener(viewListener);

                startButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences preferences = getSharedPreferences("user_details", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("first_time", true);
                        editor.apply();
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intent);

                    }
                });
            } else {
                SharedPreferences preferences = getSharedPreferences("user_details", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("details", details);
                editor.apply();

                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        }
    }

    public void addDotsIndicator(int position) {
        dots = new TextView[5];
        dotLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {

            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.gray));

            dotLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[position].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
            currentPage = i;
            if (i == 4) {
                startButton.setEnabled(true);
                startButton.setVisibility(View.VISIBLE);
            } else {
                startButton.setEnabled(false);
                startButton.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };
}
