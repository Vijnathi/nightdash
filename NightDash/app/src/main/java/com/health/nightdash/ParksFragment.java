package com.health.nightdash;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//PermissionsListener
public class ParksFragment  extends Fragment implements PermissionsListener, OnMapReadyCallback{

    private MapView mapView;
    private MapboxMap mapboxMap;
    private Style mapStyle;
    private MarkerViewManager markerViewManager;
    // variables for adding location layer
    private PermissionsManager permissionsManager;
    private LocationComponent locationComponent;

    private Point originPoint;

    // variables for calculating and drawing a route
    private DirectionsRoute currentRoute;
    private static final String TAG = "DirectionsActivity";
    private NavigationMapRoute navigationMapRoute;

    // variables needed to initialize navigation
    private Button button;
    private LinearLayout mapLayout;
    private TextView parkTitleTextView;
    private TextView parkAddressTextView;
    private RatingBar parkRatingBar;

    private LinearLayout parkDetailsLayout, parkInfoLayout;
    private TextView conditionTextView;

    View vParks;

    FrameLayout progressBarHolder;
    AlphaAnimation inAnimation;
    AlphaAnimation outAnimation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((HomeActivity)getActivity()).showStatusBar();
        Mapbox.getInstance(getActivity().getApplicationContext(), getString(R.string.access_token));
        vParks = inflater.inflate(R.layout.fragment_parks, container, false);
        mapView = (MapView) vParks.findViewById(R.id.mapView);
        mapLayout = (LinearLayout) vParks.findViewById(R.id.mapLayout);

        ((HomeActivity)getActivity()).enableBottomBar(false);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        parkTitleTextView = (TextView) vParks.findViewById(R.id.parkTitle);
        parkAddressTextView = (TextView) vParks.findViewById(R.id.parkAddress);
        parkRatingBar = (RatingBar) vParks.findViewById(R.id.parkRating);


        parkDetailsLayout = (LinearLayout) vParks.findViewById(R.id.parkDetails);
        parkInfoLayout = (LinearLayout) vParks.findViewById(R.id.parkInfo);
        conditionTextView = (TextView) vParks.findViewById(R.id.condition);
        parkDetailsLayout.setVisibility(View.GONE);
        parkInfoLayout.setVisibility(View.VISIBLE);
        conditionTextView.setVisibility(View.GONE);
        progressBarHolder = (FrameLayout) vParks.findViewById(R.id.progressBarHolder);
        progressBarHolder.setClickable(false);

        ((HomeActivity)getActivity()).disableDisplayHome();
        inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        progressBarHolder.setAnimation(inAnimation);
        progressBarHolder.setVisibility(View.VISIBLE);

        return vParks;
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                mapStyle = style;
                markerViewManager = new MarkerViewManager(mapView, mapboxMap);
                Boolean done = enableLocationComponent(style);
                if(done) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ParksAsyncTask getAllParks = new ParksAsyncTask();
                            getAllParks.execute();
                        }
                    }, 2000);

                }

                button = vParks.findViewById(R.id.startButton);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                                .directionsRoute(currentRoute)
                                .shouldSimulateRoute(true)
                                .build();
                        NavigationLauncher.startNavigation(getActivity(), options);
                    }
                });

            }
        });
    }

    private void getRoute(Point origin, Point destination) {
        ((HomeActivity)getActivity()).enableBottomBar(false);
        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(origin.latitude());
        location.setLongitude(origin.longitude());
        double bearing = Float.valueOf(location.getBearing()).doubleValue();
        double tolerance = 90d;
        NavigationRoute.builder(getActivity().getApplicationContext())
                .accessToken(getString(R.string.access_token))
                .origin(origin, bearing, tolerance)
                .destination(destination)
                .build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        // You can get the generic HTTP info about the response
                        Log.d(TAG, "Response code: " + response.code());
                        if (response.body() == null) {
                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
                            return;
                        } else if (response.body().routes().size() < 1) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                        // Draw the route on the map
                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                        button.setEnabled(true);
                        ((HomeActivity)getActivity()).enableBottomBar(true);

                        ((HomeActivity)getActivity()).enableDisplayHome();
                        outAnimation = new AlphaAnimation(1f, 0f);
                        outAnimation.setDuration(200);
                        progressBarHolder.setAnimation(outAnimation);
                        progressBarHolder.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
                        Log.e(TAG, "Error: " + throwable.getMessage());
                    }
                });
    }

    @SuppressWarnings( {"MissingPermission"})
    private Boolean enableLocationComponent(@NonNull Style loadedMapStyle) {

        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {

            // Get an instance of the component
            locationComponent = mapboxMap.getLocationComponent();

            // Activate with options
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(getActivity().getApplicationContext(), loadedMapStyle).build());

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);

            // Set the component's render mode
            locationComponent.setRenderMode(RenderMode.COMPASS);
            return true;
        } else {
            permissionsManager = new PermissionsManager(ParksFragment.this);
            permissionsManager.requestLocationPermissions(getActivity());
            return false;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(getActivity(), "Location accessed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            mapboxMap.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Toast.makeText(getActivity(), "Not granted", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mapView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    private class ParksAsyncTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
            String details = preferences.getString("details", null);
            String[] detail = details.split(" ");
            originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                    locationComponent.getLastKnownLocation().getLatitude());

            return RestClient.findAllParks(Integer.parseInt(detail[1]), originPoint.latitude(), originPoint.longitude(), detail[4]);
        }

        @Override
        protected void onPostExecute(String parks) {

                IconFactory iconFactory = IconFactory.getInstance(getActivity());
                Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.location1);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                Icon icon = iconFactory.fromBitmap(bitmap);


                try {
                    JSONObject jsonObject = new JSONObject(parks);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    List<Feature> markerCoordinates = new ArrayList<>();
                    MarkerOptions markerOptions = new MarkerOptions();
                    Boolean plotting = true;
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {

                            if(i == jsonArray.length() -1){
                                plotting = false;
                            }
                            JSONObject parkObject = jsonArray.getJSONObject(i);

                            markerOptions.position(new LatLng(parkObject.getDouble("Latitude"), parkObject.getDouble("Longitude")));
                            markerOptions.setIcon(icon);
                            mapboxMap.addMarker(markerOptions);

                            mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(@NonNull Marker marker) {

                                    Point destinationPoint = Point.fromLngLat(marker.getPosition().getLongitude(), marker.getPosition().getLatitude());
                                    Point originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                                            locationComponent.getLastKnownLocation().getLatitude());
                                    GeoJsonSource source = mapboxMap.getStyle().getSourceAs("destination-source-id");
                                    if (source != null) {
                                        source.setGeoJson(Feature.fromGeometry(destinationPoint));
                                    }

                                    ((HomeActivity)getActivity()).disableDisplayHome();
                                    inAnimation = new AlphaAnimation(0f, 1f);
                                    inAnimation.setDuration(200);
                                    progressBarHolder.setAnimation(inAnimation);
                                    progressBarHolder.setVisibility(View.VISIBLE);
                                    getRoute(originPoint, destinationPoint);
                                    mapLayout.setVisibility(View.VISIBLE);


                                    parkDetailsLayout.setVisibility(View.VISIBLE);
                                    parkInfoLayout.setVisibility(View.GONE);
                                    conditionTextView.setVisibility(View.VISIBLE);
                                    try {

                                        parkTitleTextView.setText(jsonArray.getJSONObject((int) marker.getId()).getString("FacilityName"));

                                        String address = "";
                                        String streetName = jsonArray.getJSONObject((int) marker.getId()).getString("StreetName");
                                        String suburbTown = jsonArray.getJSONObject((int) marker.getId()).getString("SuburbTown");
                                        String postcode = String.valueOf(jsonArray.getJSONObject((int) marker.getId()).getInt("Postcode"));

                                        if (!streetName.isEmpty()) {
                                            address += streetName;
                                        }
                                        if (!suburbTown.isEmpty()) {
                                            address += ", " + suburbTown;
                                        }
                                        if (!postcode.isEmpty()) {
                                            address += " - " + postcode;
                                        }

                                        parkAddressTextView.setText(address);

                                        parkRatingBar.setVisibility(View.VISIBLE);
                                        String rating = jsonArray.getJSONObject((int) marker.getId()).getString("FacilityCondition");

                                        if (rating.equalsIgnoreCase("not known")) {
                                            parkRatingBar.setRating(0);
                                        }

                                        if (rating.equalsIgnoreCase("very poor")) {
                                            parkRatingBar.setRating(1);
                                        }

                                        if (rating.equalsIgnoreCase("poor")) {
                                            parkRatingBar.setRating(2);
                                        }

                                        if (rating.equalsIgnoreCase("average")) {
                                            parkRatingBar.setRating(3);
                                        }

                                        if (rating.equalsIgnoreCase("good")) {
                                            parkRatingBar.setRating(4);
                                        }

                                        if (rating.equalsIgnoreCase("very good")) {
                                            parkRatingBar.setRating(5);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    button.setBackgroundResource(R.color.colorAccent);
                                    return true;
                                }
                            });
                        }

                        ((HomeActivity)getActivity()).enableDisplayHome();
                        outAnimation = new AlphaAnimation(1f, 0f);
                        outAnimation.setDuration(200);
                        progressBarHolder.setAnimation(outAnimation);
                        progressBarHolder.setVisibility(View.GONE);
                    } else {
                        ((HomeActivity)getActivity()).enableDisplayHome();
                        outAnimation = new AlphaAnimation(1f, 0f);
                        outAnimation.setDuration(200);
                        progressBarHolder.setAnimation(outAnimation);
                        progressBarHolder.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage("Parks are not available within specified radius and surface type. Change values and try. Thank you.");
                        alertDialogBuilder.setPositiveButton("Done",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        ((HomeActivity)getActivity()).enableBottomBar(true);
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }

                    if(!plotting) {
                        ((HomeActivity)getActivity()).enableBottomBar(true);
                    }

                } catch(Exception e){
                    e.printStackTrace();
                }
        }
    }

}
