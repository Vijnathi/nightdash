package com.health.nightdash;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class DashboardRestClient {
    private static final String BASE_URL_NUMBER_PARK = "https://2cy4cgv044.execute-api.ap-southeast-2.amazonaws.com/GetNumberOfParks/";
    private static final String BASE_URL_NUMBER_ROUTE = "https://eat3egophi.execute-api.ap-southeast-2.amazonaws.com/GetNumberOfPaths/";
    private static final String BASE_URL_NUMBER_CRIME = "https://is8oymx2ui.execute-api.ap-southeast-2.amazonaws.com/GetNumberOfCrimes/";
    private static final String BASE_URL_NUMBER_CRASH = "https://lkhbhv1czj.execute-api.ap-southeast-2.amazonaws.com/GetNumberOfCrashes/";

    public static String findNumberOfParks(int radius, Double latitude, Double longitude, String surface_type) {
        final String methodPath = (latitude + "/" + longitude + "/" + radius + "/" + surface_type);
        //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        //Making HTTP request
        try {
            url = new URL(BASE_URL_NUMBER_PARK + methodPath);
            //open the connection
            conn = (HttpURLConnection) url.openConnection(); //set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000); //set the connection method to GET
            conn.setRequestMethod("GET"); //add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return textResult;
    }

    public static String findNumberOfPaths(int radius, Double latitude, Double longitude) {
        final String methodPath = (latitude + "/" + longitude + "/" + radius);
        //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        //Making HTTP request
        try {
            url = new URL(BASE_URL_NUMBER_ROUTE + methodPath);
            //open the connection
            conn = (HttpURLConnection) url.openConnection(); //set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000); //set the connection method to GET
            conn.setRequestMethod("GET"); //add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return textResult;
    }

    public static String getNumberOfCrash(int radius, Double latitude, Double longitude) {
        final String methodPath = (radius + "/" +latitude + "/" + longitude);
        //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        //Making HTTP request
        try {
            url = new URL(BASE_URL_NUMBER_CRASH + methodPath);
            //open the connection
            conn = (HttpURLConnection) url.openConnection(); //set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000); //set the connection method to GET
            conn.setRequestMethod("GET"); //add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return textResult;
    }

    public static String getNumberOfCrimes(int postcode) {
//        final String methodPath = (postcode);
        //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        //Making HTTP request
        try {
            url = new URL(BASE_URL_NUMBER_CRIME + postcode);
            //open the connection
            conn = (HttpURLConnection) url.openConnection(); //set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000); //set the connection method to GET
            conn.setRequestMethod("GET"); //add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return textResult;
    }

    public static int getCountFromData(String data) {

        int count = 0;
        try{
            JSONObject object = new JSONObject(data);
            if(object != null) {
                JSONArray array = object.getJSONArray("data");
                if (array.length() > 0) {
                    String countStr = array.getJSONObject(0).getString("Count");
                    count = Integer.parseInt(countStr);
                    return count;
                }
            }
            return count;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
}
