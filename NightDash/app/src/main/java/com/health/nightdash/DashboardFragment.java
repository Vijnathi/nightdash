package com.health.nightdash;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DashboardFragment extends Fragment implements LocationListener, PermissionsListener{
    View vDashboard;

    private TextView welcomeMessageTextView;
    private TextView remainingTimeTextView;
    private CircularProgressBar progressBar;

    private TextView parksCountTextView;
    private TextView routesCountTextView;
    private TextView crimesCountTextView;
    private TextView crashesCountTextView;

    private RelativeLayout showParksLayout;
    private RelativeLayout showRoutesLayout;

    private Button fuelUp;

    private LocationManager locationManager;
    private PermissionsManager permissionsManager;

    private Double latitude;
    private Double longitude;

    AlphaAnimation inAnimation;
    AlphaAnimation outAnimation;

    FrameLayout progressBarHolder;

    private boolean parksDone;
    private boolean crimesDone;
    private boolean crashesDone;
    private boolean routesDone;
    private boolean weatherDone;

    private ImageButton setRunImageButton;

    private TextView foodMessageTextView;

    private ImageView weatherImageView;
    private TextView temperatureTextView, feelTemperatureTextView, rainTextView, weatherStatusTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).hideStatusBar();
        vDashboard = inflater.inflate(R.layout.fragment_dashboard, container, false);

        parksDone = false;
        crimesDone = false;
        crashesDone = false;
        routesDone = false;
        weatherDone = true;


        welcomeMessageTextView = (TextView) vDashboard.findViewById(R.id.welcomeMessage);
        remainingTimeTextView = (TextView) vDashboard.findViewById(R.id.remainingTime);
        progressBar = vDashboard.findViewById(R.id.progress_bar);

        parksCountTextView = (TextView) vDashboard.findViewById(R.id.parksCount);
        routesCountTextView = (TextView) vDashboard.findViewById(R.id.routesCount);
        crimesCountTextView = (TextView) vDashboard.findViewById(R.id.crimesCount);
        crashesCountTextView = (TextView) vDashboard.findViewById(R.id.crashesCount);

        weatherImageView = (ImageView) vDashboard.findViewById(R.id.weatherImage);
        temperatureTextView = (TextView) vDashboard.findViewById(R.id.temperature);
        feelTemperatureTextView = (TextView) vDashboard.findViewById(R.id.feelTemperature);
        rainTextView = (TextView) vDashboard.findViewById(R.id.rain);
        weatherStatusTextView = (TextView) vDashboard.findViewById(R.id.weatherStatus);

        foodMessageTextView = (TextView) vDashboard.findViewById(R.id.foodMessage);

        ((HomeActivity)getActivity()).enableBottomBar(false);
        progressBarHolder = (FrameLayout) vDashboard.findViewById(R.id.progressBarHolder);
        progressBarHolder.setClickable(false);
        inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        progressBarHolder.setAnimation(inAnimation);
        progressBarHolder.setVisibility(View.VISIBLE);

        displayWelcomeMessageWithTimings();
        displayRemainingTime();

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            permissionsManager = new PermissionsManager(DashboardFragment.this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        showParksLayout = (RelativeLayout) vDashboard.findViewById(R.id.showParks);
        showParksLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).navigate(R.id.navigation_parks);
            }
        });

        showRoutesLayout = (RelativeLayout) vDashboard.findViewById(R.id.showRoutes);
        showRoutesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).navigate(R.id.navigation_route);
            }
        });
        setRunImageButton = (ImageButton) vDashboard.findViewById(R.id.setRunImageButton);
        setRunImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).navigate(R.id.navigation_profile);
            }
        });
        fuelUp = (Button) vDashboard.findViewById(R.id.fuelUpButton);
        fuelUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).navigate(R.id.navigation_fuelUp);
            }
        });

        showParksLayout.setEnabled(false);
        showRoutesLayout.setEnabled(false);
        setRunImageButton.setEnabled(false);
        Double[] locData = {latitude, longitude};

        new Handler().postDelayed(new Runnable() {
             @Override
             public void run() {
                 new GetPathsCount().execute(locData);
                 new GetParksCount().execute(locData);
                 new GetCrashesCount().execute(locData);
                 new GetCrimesCount().execute(locData);
                 new GetWeather().execute("");
             }
        }, 3000);

//        SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
//        String parksCount = preferences.getString("parksCount", "");
//        String pathsCount = preferences.getString("pathsCount", "");
//        String crimesCount = preferences.getString("crimesCount", "");
//        String crashesCount = preferences.getString("crashesCount", "");
//        String weatherData = preferences.getString("weahtherData", "");
//        if(parksCount.isEmpty() && pathsCount.isEmpty() && crimesCount.isEmpty() && crashesCount.isEmpty()) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = preferences.edit();
//                    editor.putString("lat", String.valueOf(latitude));
//                    editor.putString("lon", String.valueOf(longitude));
//                    editor.apply();
//                    new GetPathsCount().execute(locData);
//                    new GetParksCount().execute(locData);
//                    new GetCrashesCount().execute(locData);
//                    new GetCrimesCount().execute(locData);
//                    new GetWeather().execute("");
//                }
//            }, 3000);
//        } else {
//
//            String lat = preferences.getString("lat", "");
//            String lon = preferences.getString("lon", "");
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if(lat != "" && lon != "") {
//                        if (Double.parseDouble(lat) == latitude && Double.parseDouble(lon) == longitude) {
//                            parksCountTextView.setText(parksCount);
//                            routesCountTextView.setText(pathsCount);
//                            crashesCountTextView.setText(crashesCount);
//                            crimesCountTextView.setText(crimesCount);
//
//                            // throwing error
//                            String[] data = weatherData.split(" ");
//                            rainTextView.setText(data[3]);
//                            temperatureTextView.setText(data[1]);
//                            feelTemperatureTextView.setText(data[2]);
//                            weatherStatusTextView.setText(data[4]);
//
//                            new GetImageFromUrl(weatherImageView).execute("http://openweathermap.org/img/wn/" + data[0] + "@2x.png");
//
//                            ((HomeActivity) getActivity()).enableBottomBar(true);
//
//                            outAnimation = new AlphaAnimation(1f, 0f);
//                            outAnimation.setDuration(200);
//                            progressBarHolder.setAnimation(outAnimation);
//                            progressBarHolder.setVisibility(View.GONE);
//                            progressBarHolder.setClickable(true);
//
//                            showParksLayout.setEnabled(true);
//                            showRoutesLayout.setEnabled(true);
//                            setRunImageButton.setEnabled(true);
//                        } else {
//                            new GetPathsCount().execute(locData);
//                            new GetParksCount().execute(locData);
//                            new GetCrashesCount().execute(locData);
//                            new GetCrimesCount().execute(locData);
//                            new GetWeather().execute("");
//                        }
//                    }
//                }
//            }, 3000);

//        }
//
        return vDashboard;
    }

    public void displayWelcomeMessageWithTimings(){

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String dateToStr = format.format(today);
        System.out.println(dateToStr);
        String message = "Good ";

        int hour = Integer.parseInt(dateToStr.substring(0, 2));
        if(hour >= 5 & hour <= 12) {
            message += "Morning";
        }
        if ((hour >= 0 & hour < 5) | (hour >= 18 & hour <= 23)) {
            message += "Evening";
        }
        if (hour > 12 & hour < 18) {
            message += "Afternoon";
        }
        welcomeMessageTextView.setText(message);
    }

    public void displayRemainingTime() {

        String timeStart = "05:00";

        SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
        String details = preferences.getString("details", null);
        String[] detailsArr = details.split(" ");

        String timeScheduled = detailsArr[3];

        Date today = new Date();

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String timeCurrent = format.format(today);
//        String timeCurrent = "16:30";
        Date startTime = null;
        Date scheduledTime = null;
        Date currentTime = null;

        try {
            startTime = format.parse(timeStart);
            scheduledTime = format.parse(timeScheduled);
            currentTime = format.parse(timeCurrent);

            //in milliseconds
            long diffMain = scheduledTime.getTime() - startTime.getTime();
            long diffCurrent = scheduledTime.getTime() - currentTime.getTime();

            long diffMainSeconds = diffMain / 1000 % 60;
            long diffMainMinutes = diffMain / (60 * 1000) % 60;
            long diffMainHours = diffMain / (60 * 60 * 1000) % 24;

            long diffCurrentSeconds = diffCurrent / 1000 % 60;
            long diffCurrentMinutes = diffCurrent / (60 * 1000) % 60;
            long diffCurrentHours = diffCurrent / (60 * 60 * 1000) % 24;

            long max = (diffMainHours * 60) + diffMainMinutes;
            long value = (diffCurrentHours * 60) + diffCurrentMinutes;

            progressBar.setMaximum(max);

            if (value == 0) {
                remainingTimeTextView.setText("00:00:" + String.valueOf(diffCurrentSeconds)+"\nleft for\nyour run");
                progressBar.setForegroundStrokeColor(getResources().getColor(R.color.white));
                progressBar.setProgress(max-value);
//                }
            } else if(value < 60 & value > 0) {
                remainingTimeTextView.setText("00:"+String.valueOf(value)+":"+String.valueOf(diffCurrentSeconds)+"\nleft for\nyour run");
                progressBar.setForegroundStrokeColor(getResources().getColor(R.color.white));
                progressBar.setProgress(max-value);
            } else if(max-value < 0 | value < 0) {
                remainingTimeTextView.setText("00:00:00"+"\nleft for\nyour run");
                progressBar.setForegroundStrokeColor(getResources().getColor(R.color.white));
                progressBar.setProgress(max);
            } else if((value % 60) == 0) {
                progressBar.setForegroundStrokeColor(getResources().getColor(R.color.white));
                progressBar.setProgress(max-value);
                remainingTimeTextView.setText(String.valueOf(diffCurrentHours)+":00:00"+"\nleft for\nyour run");
            } else {
                progressBar.setProgress(max-value);
                progressBar.setForegroundStrokeColor(getResources().getColor(R.color.white));
                remainingTimeTextView.setText(String.valueOf(diffCurrentHours) + ":" + String.valueOf(diffCurrentMinutes) + ":" + String.valueOf(diffCurrentSeconds)+"\nleft for\nyour run");
            }
            String message = "";
            if(diffCurrentHours < 2) {
                message = "Time left for for your run is less than 2 hours.\nBetter not to have any food \nor have a very light food such as juices.\n\nIf you have heavier food, you will feel discomfort while running\n and might also face health issues.";
            }
            if(diffCurrentHours == 2) {

                message = "To top off your stomach, eat a light snack (up to 200-300 calorie) consisting mainly of easily digestible carbohydrates such as a slice of toast with almond butter and a banana, or an alternate versions. If you still feel hungry, take with you some food chews or gels before the run.";
            }
            if(diffCurrentHours == 3 || diffCurrentHours == 4) {
                message = "Eat a meal (mostly second meal of your day), which is easily digestible, made up of carbohydrates and low in fibre. This meal (around 500 calories) is your lunch, such as sandwich, pretzels, a fruit, or an alternate versions. No Greens";
            }
            if(diffCurrentHours == 5 || diffCurrentHours == 6) {
                message = "This time will be between your lunch and breakfast. You would be fairly full from your breakfast, so eat a light snack such as a juice, a fruit, almonds, or an alternate versions. If you still feel hungry, take with you some food chews or gels.";
            }
            if(diffCurrentHours == 7 || diffCurrentHours == 8) {
                message = "Now, it would be around your breakfast time. So have a good-sized breakfast (800 calories or more) such as small pancakes, waffle with bacon on both sides, scrambled egg(s), or alternate versions. You can also include proteins and fat, if you want, in your breakfast. But avoid foods which digest slowly.";
            }
            if(diffCurrentHours > 8) {
                message = "You can eat anything now. But don't have heavy food if you are around your breakfast time. Lighter food can hold you until your breakfast. If you are too hungry, you can finish up your breakfast.";
            }

            foodMessageTextView.setText(message);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {

        }
    }

    private class GetParksCount extends AsyncTask<Double, Void, String> {

        @Override
        protected String doInBackground(Double... params) {
            SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
            String details = preferences.getString("details", null);
            String[] detailsArr = details.split(" ");

            return DashboardRestClient.findNumberOfParks(Integer.parseInt(detailsArr[1]), latitude, longitude, detailsArr[4]);
        }

        @Override
        protected void onPostExecute(String parks) {
            Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.content_frame);
            int count = DashboardRestClient.getCountFromData(parks);
            if (currentFragment instanceof DashboardFragment) {
                parksCountTextView.setText(String.valueOf(count));
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("parksCount", String.valueOf(count));
                editor.apply();
                parksDone = true;
            } else {
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("parksCount", String.valueOf(count));
                editor.apply();
                parksDone = true;
            }

            if(parksDone && crashesDone && crimesDone && routesDone && weatherDone) {
                outAnimation = new AlphaAnimation(1f, 0f);
                outAnimation.setDuration(200);
                progressBarHolder.setAnimation(outAnimation);
                progressBarHolder.setVisibility(View.GONE);
                ((HomeActivity)getActivity()).enableBottomBar(true);
                showParksLayout.setEnabled(true);
                showRoutesLayout.setEnabled(true);
                setRunImageButton.setEnabled(true);
            }
        }
    }

    private class GetPathsCount extends AsyncTask<Double, Void, String> {

        @Override
        protected String doInBackground(Double... params) {
            SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
            String details = preferences.getString("details", null);
            String[] detailsArr = details.split(" ");

            return DashboardRestClient.findNumberOfPaths(Integer.parseInt(detailsArr[1]), latitude, longitude);
        }

        @Override
        protected void onPostExecute(String paths) {
            Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.content_frame);
            int count = DashboardRestClient.getCountFromData(paths);
            if (currentFragment instanceof DashboardFragment) {
                routesCountTextView.setText(String.valueOf(count));
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("pathsCount", String.valueOf(count));
                editor.apply();
                routesDone = true;
            } else {
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("pathsCount", String.valueOf(count));
                editor.apply();
                routesDone = true;
            }

            if(parksDone && crashesDone && crimesDone && routesDone && weatherDone) {
                outAnimation = new AlphaAnimation(1f, 0f);
                outAnimation.setDuration(200);
                progressBarHolder.setAnimation(outAnimation);
                progressBarHolder.setVisibility(View.GONE);
                progressBarHolder.setClickable(true);
                ((HomeActivity)getActivity()).enableBottomBar(true);
                showParksLayout.setEnabled(true);
                showRoutesLayout.setEnabled(true);
                setRunImageButton.setEnabled(true);
            }
        }
    }

    private class GetCrimesCount extends AsyncTask<Double, Void, String> {

        @Override
        protected String doInBackground(Double... params) {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                return DashboardRestClient.getNumberOfCrimes(Integer.parseInt(addresses.get(0).getPostalCode()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String crimes) {
            if(!crimes.equals("")) {
                Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.content_frame);
                int count = DashboardRestClient.getCountFromData(crimes);
                if (currentFragment instanceof DashboardFragment) {
                    crimesCountTextView.setText(String.valueOf(count));
                    SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("crimesCount", String.valueOf(count));
                    editor.apply();
                    crimesDone = true;
                } else {
                    SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("crimesCount", String.valueOf(count));
                    editor.apply();
                    crimesDone = true;
                }
                if(parksDone && crashesDone && crimesDone && routesDone && weatherDone) {
                    outAnimation = new AlphaAnimation(1f, 0f);
                    outAnimation.setDuration(200);
                    progressBarHolder.setAnimation(outAnimation);
                    progressBarHolder.setVisibility(View.GONE);
                    ((HomeActivity)getActivity()).enableBottomBar(true);
                    showParksLayout.setEnabled(true);
                    showRoutesLayout.setEnabled(true);
                    setRunImageButton.setEnabled(true);
                }
            } else {
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("crimesCount", "");
            }
        }
    }

    private class GetCrashesCount extends AsyncTask<Double, Void, String> {

        @Override
        protected String doInBackground(Double... params) {
            return DashboardRestClient.getNumberOfCrash(250, latitude, longitude);
        }

        @Override
        protected void onPostExecute(String crashes) {

            Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.content_frame);
            int count = DashboardRestClient.getCountFromData(crashes);
            if (currentFragment instanceof DashboardFragment) {
                crashesCountTextView.setText(String.valueOf(count));
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("crashesCount", String.valueOf(count));
                editor.apply();
                crashesDone = true;
            } else {
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("crashesCount", String.valueOf(count));
                editor.apply();
                crashesDone = true;
            }

            if(parksDone && crashesDone && crimesDone && routesDone && weatherDone) {
                outAnimation = new AlphaAnimation(1f, 0f);
                outAnimation.setDuration(200);
                progressBarHolder.setAnimation(outAnimation);
                progressBarHolder.setVisibility(View.GONE);
                ((HomeActivity)getActivity()).enableBottomBar(true);
                showParksLayout.setEnabled(true);
                showRoutesLayout.setEnabled(true);
                setRunImageButton.setEnabled(true);
            }
        }
    }

    private class GetWeather extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return WeatherRestClient.getWeatherData(latitude, longitude);
        }

        @Override
        protected void onPostExecute(String weatherData) {

            try {
                JSONObject object = new JSONObject(weatherData);
                JSONArray array = object.getJSONArray("weather");
                String weatherStatus = array.getJSONObject(0).getString("description");
                String[] strArray = weatherStatus.split(" ");
                StringBuilder builder = new StringBuilder();
                for (String s : strArray) {
                    String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                    builder.append(cap + " ");
                }
                weatherStatus = builder.toString();

                String icon = array.getJSONObject(0).getString("icon");

                JSONObject main = object.getJSONObject("main");
                Double maxTemp = main.getDouble("temp_max");
                Double minTemp = main.getDouble("temp_min");

                JSONObject wind = object.getJSONObject("wind");
                Double speed = wind.getDouble("speed");

                String rainText = "Wind "+String.valueOf(speed)+" m/s";
                String tempText = "Max. "+maxTemp.intValue()+"\u2103";
                String feelTempText = "Min. "+minTemp.intValue()+"\u2103";

                Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.content_frame);
                if (currentFragment instanceof DashboardFragment) {
                    rainTextView.setText(rainText);
                    temperatureTextView.setText(tempText);
                    feelTemperatureTextView.setText(feelTempText);
                    weatherStatusTextView.setText(weatherStatus);

                    new GetImageFromUrl(weatherImageView).execute("http://openweathermap.org/img/wn/"+icon+"@2x.png");

                    SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("weatherData", icon + " " + tempText + " " + feelTempText + " " + rainText + " " + weatherStatus);
                    editor.apply();
                    weatherDone = true;
                } else {
                    SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("weatherData", icon + " " + tempText + " " + feelTempText + " " + rainText + " " + weatherStatus);
                    editor.apply();
                    weatherDone = true;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            if(parksDone && crashesDone && crimesDone && routesDone && weatherDone) {
                outAnimation = new AlphaAnimation(1f, 0f);
                outAnimation.setDuration(200);
                progressBarHolder.setAnimation(outAnimation);
                progressBarHolder.setVisibility(View.GONE);
                ((HomeActivity)getActivity()).enableBottomBar(true);
                showParksLayout.setEnabled(true);
                showRoutesLayout.setEnabled(true);
                setRunImageButton.setEnabled(true);
            }
        }
    }

    private class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {

        ImageView imageView;
        public GetImageFromUrl(ImageView imageView) {
            this.imageView = imageView;
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(params[0]).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(Bitmap.createScaledBitmap(result, 120, 120, false));
        }
    }

}
