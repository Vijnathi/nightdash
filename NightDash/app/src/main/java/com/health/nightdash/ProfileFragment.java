package com.health.nightdash;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ProfileFragment extends Fragment {
    private View vProfile;

    private UserDetailsDatabase db = null;

    private ScrollView enterDetailsLayout;
    private RelativeLayout enterDetails;
    private RelativeLayout displayDetailsLayout;
    private LinearLayout groundScrollView;

    private EditText radiusEditText;
    private EditText timeEditText;
    private Spinner groundTypeSpinner;

    private TextView radiusErrorTextView;
    private TextView timeErrorTextView;
    private TextView groundTypeErrorTextView;

    private TextView distanceDisplayValueTextView;
    private TextView timeDisplayValueTextView;
    private TextView groundTypeDisplayValueTextView;

    private TimePickerDialog picker;
    private String selectedgroundType;

    private Button saveButton;
    private Button editButton, homeButton;
    private Button groundTypeHelpButton;
    private Button helpBackButton;

    private UserDetails userDetails;

    private String page;
    private String groundTypeJson = null;

    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).showStatusBar();
        vProfile = inflater.inflate(R.layout.fragment_profile, container, false);

        db = Room.databaseBuilder(getActivity().getApplicationContext(), UserDetailsDatabase.class, "UserDetailsDatabase").fallbackToDestructiveMigration().build();

        distanceDisplayValueTextView = (TextView) vProfile.findViewById(R.id.distanceDisplayValue);
        timeDisplayValueTextView = (TextView) vProfile.findViewById(R.id.timeDisplayValue);
        groundTypeDisplayValueTextView = (TextView) vProfile.findViewById(R.id.groundTypeDisplayValue);

        enterDetailsLayout = (ScrollView) vProfile.findViewById(R.id.enterDetailsLayout);
        enterDetails = (RelativeLayout) vProfile.findViewById(R.id.enterDetails);
        displayDetailsLayout = (RelativeLayout) vProfile.findViewById(R.id.displayDetails);
        groundScrollView = (LinearLayout) vProfile.findViewById(R.id.groundTypeHelp);

        SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
        String details = preferences.getString("details", null);

        enterDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                if(getActivity().getCurrentFocus() != null) {
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
            }
        });
        if (details == null) {
            ((HomeActivity)getActivity()).enableBottomBar(false);
            ((HomeActivity)getActivity()).disableDisplayHome();
            enterDetailsLayout.setVisibility(View.VISIBLE);
            displayDetailsLayout.setVisibility(View.GONE);
            groundScrollView.setVisibility(View.GONE);
            page = "enter";

        } else {
            ((HomeActivity)getActivity()).enableBottomBar(true);
            String[] detailsArr = details.split(" ");
            userDetails = new UserDetails();
            userDetails.setUserDetailsid(Integer.parseInt(detailsArr[0]));
            userDetails.setRadius(Integer.parseInt(detailsArr[1]));
            userDetails.setDistance(Integer.parseInt(detailsArr[2]));
            userDetails.setTime(detailsArr[3]);
            userDetails.setSurface_type(detailsArr[4]);
            userDetails.setFirst_time(Boolean.parseBoolean(detailsArr[5]));
            ((HomeActivity)getActivity()).enableDisplayHome();
            enterDetailsLayout.setVisibility(View.GONE);
            displayDetailsLayout.setVisibility(View.VISIBLE);
            groundScrollView.setVisibility(View.GONE);
            distanceDisplayValueTextView.setText(String.valueOf((userDetails.getRadius() / 1000.0)) + " KM");
            timeDisplayValueTextView.setText(userDetails.getTime());
            groundTypeDisplayValueTextView.setText(userDetails.getSurface_type());
            page = "display";
        }


        radiusErrorTextView = (TextView) vProfile.findViewById(R.id.distanceInputError);
        timeErrorTextView = (TextView) vProfile.findViewById(R.id.timeInputError);
        groundTypeErrorTextView = (TextView) vProfile.findViewById(R.id.groundTypeError);

        radiusEditText = (EditText) vProfile.findViewById(R.id.distanceInput);
        radiusEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String radiusStr = radiusEditText.getText().toString();
                if (radiusStr.equals("") | radiusStr == null | radiusStr.equals(" ")) {
                    radiusErrorTextView.setText("Please enter radius");
                } else {

                    radiusErrorTextView.setText("");
                    Double radiusKms = Double.parseDouble(radiusStr);
                    int radius = (int) (radiusKms * 1000);

                    if (radiusKms > 3.0) {
                        radiusErrorTextView.setText("Radius should be lesser than 3kms");
                    } else {
                        radiusErrorTextView.setText("");
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        timeEditText = (EditText) vProfile.findViewById(R.id.timeInput);

        timeEditText.setInputType(InputType.TYPE_NULL);
        timeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);

                // time picker dialog
                picker = new TimePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hour, int minutes) {
                                timeEditText.setText(hour + ":" + minutes);
                            }
                        }, hour, minutes, true);
                picker.show();
            }
        });
        timeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String timeStr = timeEditText.getText().toString();
                if (timeStr.equals("") | timeStr == null | timeStr.equals(" ")) {
//                    error = true;
                    timeErrorTextView.setText("Please enter time");
                } else {
                    timeErrorTextView.setText("");
                    DateFormat sdf = new SimpleDateFormat("hh:mm");

                    Date startTime = null;
                    Date endTime = null;
                    Date time = null;

                    try {
                        startTime = sdf.parse("17:30");
                        endTime = sdf.parse("23:00");
                        time = sdf.parse(timeStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (time != null & startTime != null & endTime != null) {
                        if (time.after(endTime) | time.before(startTime)) {
                            timeErrorTextView.setText("Time should be between 17:30 - 23:00");
                        } else {
                            timeErrorTextView.setText("");
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        List<String> groundTypeList = new ArrayList<String>();
        groundTypeList.add("Select");
        groundTypeList.add("Grass");
        groundTypeList.add("Dirt");
        groundTypeList.add("Track");
        groundTypeList.add("Asphalt");
        groundTypeList.add("Concrete");

        groundTypeSpinner = (Spinner) vProfile.findViewById(R.id.groundTypeSpinner);
        final ArrayAdapter<String> groundTypeSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, groundTypeList);
        groundTypeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groundTypeSpinner.setAdapter(groundTypeSpinnerAdapter);
        groundTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedgroundType = parent.getItemAtPosition(position).toString();
                if (selectedgroundType != null & !selectedgroundType.equals("Select")) {
                    groundTypeErrorTextView.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        loadJsonManual();
        saveButton = (Button) vProfile.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean error = false;
                Double radiusKms = null;
                int radius = 0;

                String radiusStr = radiusEditText.getText().toString();
                String timeStr = timeEditText.getText().toString();

                if (radiusStr.equals("") | radiusStr == null | radiusStr.equals(" ")) {
                    error = true;
                    radiusErrorTextView.setText("Please enter radius");
                } else {

                    radiusErrorTextView.setText("");
                    radiusKms = Double.parseDouble(radiusStr);
                    radius = (int) (radiusKms * 1000);

                    if (radiusKms > 3.0) {
                        error = true;
                        radiusErrorTextView.setText("Radius should be lesser than 3kms");
                    } else {
                        radiusErrorTextView.setText("");
                    }

                }
                if (timeStr.equals("") | timeStr == null | timeStr.equals(" ")) {
                    error = true;
                    timeErrorTextView.setText("Please enter time");
                } else {
                    timeErrorTextView.setText("");
                    DateFormat sdf = new SimpleDateFormat("hh:mm");

                    Date startTime = null;
                    Date endTime = null;
                    Date time = null;

                    try {
                        startTime = sdf.parse("17:30");
                        endTime = sdf.parse("23:00");
                        time = sdf.parse(timeStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (time != null & startTime != null & endTime != null) {
                        if (time.after(endTime) | time.before(startTime)) {
                            error = true;
                            timeErrorTextView.setText("Time should be between 17:30 - 23:00");
                        } else {
                            timeErrorTextView.setText("");
                        }
                    }
                }
                if (selectedgroundType == null | selectedgroundType.equals("Select")) {
                    error = true;
                    groundTypeErrorTextView.setText("Please select a ground type");
                }


                if (!error) {
                    Integer[] data = {radius, 1000};
                    if (page.equals("enter")) {
                        InsertDatabase addDatabase = new InsertDatabase();
                        addDatabase.execute(data);
                    } else {
                        UpdateDatabase updateDatabase = new UpdateDatabase();
                        updateDatabase.execute(data);
                    }
                }

            }
        });

        editButton = (Button) vProfile.findViewById(R.id.editButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                String detail = pref.getString("details", null);
                String[] detailArr = detail.split(" ");
                radiusEditText.setText(String.valueOf(Double.parseDouble(detailArr[1]) / 1000.0));
//                distanceEditText.setText(String.valueOf(Double.parseDouble(detailArr[2]) / 1000.0));
                timeEditText.setText(detailArr[3]);
                for (String type : groundTypeList) {
                    if (type.equals(detailArr[4])) {
                        groundTypeSpinner.setSelection(groundTypeList.indexOf(type));
                    }
                }

                ((HomeActivity)getActivity()).disableDisplayHome();
                enterDetailsLayout.setVisibility(View.VISIBLE);
                displayDetailsLayout.setVisibility(View.GONE);
                groundScrollView.setVisibility(View.GONE);
            }
        });

        groundTypeHelpButton = (Button) vProfile.findViewById(R.id.groundTypeHelpButton);
        groundTypeHelpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).disableDisplayHome();
                enterDetailsLayout.setVisibility(View.GONE);
                displayDetailsLayout.setVisibility(View.GONE);
                groundScrollView.setVisibility(View.VISIBLE);
            }
        });

        ArrayList<DataModel> _listDataModel = listArray();

        recyclerView = (RecyclerView) vProfile.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        /* Set Horizontal LinearLayout to RecyclerView */
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(mLayoutManager);

        /* Set MyCustomAdapter to RecyclerView */
        recyclerView.setAdapter(new CustomCardAdapter(_listDataModel));
        helpBackButton = (Button) vProfile.findViewById(R.id.helpBackButton);
        helpBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).disableDisplayHome();
                enterDetailsLayout.setVisibility(View.VISIBLE);
                displayDetailsLayout.setVisibility(View.GONE);
                groundScrollView.setVisibility(View.GONE);
            }
        });

        homeButton = (Button) vProfile.findViewById(R.id.homeButton);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).navigate(R.id.navigation_dashboard);
            }
        });

        return vProfile;
    }

    private class InsertDatabase extends AsyncTask<Integer, Void, String> {

        @Override
        protected String doInBackground(Integer... params) {

            SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
            Boolean first_time = preferences.getBoolean("first_time", false);
            if (first_time) {
                UserDetails userDetails = new UserDetails(params[0], params[1], timeEditText.getText().toString(), selectedgroundType, false);
                long id = db.userDetailsDAO().insert(userDetails);
                return (id + " " + params[0] + " " + params[1] + " " + timeEditText.getText().toString() + " " + selectedgroundType + " " + false);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String details) {
            if (details != null) {
                String[] detailsArr = details.split(" ");
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("details", details);
                editor.putBoolean("first_time", false);
                editor.apply();

                distanceDisplayValueTextView.setText(String.valueOf((Double.parseDouble(detailsArr[1]) / 1000.0)) + " KM");
                timeDisplayValueTextView.setText(detailsArr[3]);
                groundTypeDisplayValueTextView.setText(detailsArr[4]);

                ((HomeActivity)getActivity()).enableDisplayHome();
                enterDetailsLayout.setVisibility(View.GONE);
                displayDetailsLayout.setVisibility(View.VISIBLE);
                groundScrollView.setVisibility(View.GONE);

                ((HomeActivity)getActivity()).enableBottomBar(true);
                ((HomeActivity)getActivity()).setAlarm();
            }
        }

    }

    private class UpdateDatabase extends AsyncTask<Integer, Void, String> {
        @Override
        protected String doInBackground(Integer... params) {

            UserDetails userDetail = null;
            userDetail = new UserDetails();
            userDetail.setUserDetailsid(userDetails.getUserDetailsid());
            userDetail.setRadius(params[0]);
            userDetail.setDistance(params[1]);
            userDetail.setTime(timeEditText.getText().toString());
            userDetail.setSurface_type(selectedgroundType);
            userDetail.setFirst_time(userDetails.getFirst_time());

            if (userDetail != null) {
                db.userDetailsDAO().updateUsers(userDetail);
                return (userDetails.getUserDetailsid() + " " + params[0] + " " + params[1] + " " + timeEditText.getText().toString() + " " + selectedgroundType + " " + userDetails.getFirst_time());
            }
            return "";

        }

        @Override
        protected void onPostExecute(String details) {
            if (details != "") {
                String[] detailsArr = details.split(" ");
                SharedPreferences preferences = getActivity().getSharedPreferences("user_details", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("details", details);
                editor.apply();

                distanceDisplayValueTextView.setText(String.valueOf((Double.parseDouble(detailsArr[1]) / 1000.0)) + " KM");
                timeDisplayValueTextView.setText(detailsArr[3]);
                groundTypeDisplayValueTextView.setText(detailsArr[4]);

                ((HomeActivity)getActivity()).enableDisplayHome();
                enterDetailsLayout.setVisibility(View.GONE);
                displayDetailsLayout.setVisibility(View.VISIBLE);
                groundScrollView.setVisibility(View.GONE);

                ((HomeActivity)getActivity()).enableBottomBar(true);
            }
        }

    }

    public void loadJsonManual() {

        try {

            InputStream is = getResources().openRawResource(R.raw.ground_type);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            groundTypeJson = new String(buffer, "UTF-8");

            System.out.println(groundTypeJson);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<DataModel> listArray() {

        ArrayList<DataModel> objList = new ArrayList<DataModel>();
        DataModel dm;

        dm = new DataModel();
        dm.setGroundType("Grass");
        dm.setPros("Grass is soft and low impact, so it may be a better choice for people who have impact-related running injuries. It's usually rated as one of the best surfaces for running.");
        dm.setCons("A run in the park can be a little stressful! Besides hidden holes, rocks, and twigs, you also have to watch out for other obstacles, like pedestrians, dogs, and other distractions.");
        dm.setNote("Not paying attention when running on grass commonly leads to injuries like a twisted ankle, so make sure you keep aware of both the ground directly in front of you as well as the ground ahead.");
        dm.setImgSrc(R.drawable.grass);
        objList.add(dm);

        dm = new DataModel();
        dm.setGroundType("Asphalt");
        dm.setPros("Since most races are run on the street, if you are training for a race you should get off the treadmill, so you will be more in tune with any obstacles in the streets. Also, running on the street can be better for those who experience Achilles tendonitis, since the sturdy surface keeps your Achilles tendon in a less-tensed position.");
        dm.setCons("The road is made up of many different obstacles and dangers, from potholes to cars, which can make your running more unsafe.");
        dm.setNote("When running on the road, always wear bright clothes and make sure you turn your music down low if you wear headphones so you can be aware of what's going on around you.");
        dm.setImgSrc(R.drawable.asphalt);
        objList.add(dm);

        dm = new DataModel();
        dm.setGroundType("Concrete");
        dm.setPros("Running on the sidewalk can be the most convenient if you live in a city, and it also may be the safest option if you don't want to risk it on the road.");
        dm.setCons("Concrete sidewalks are one of the hardest surfaces you can run on, which may translate to more stress on your joints and muscles. Most running experts recommend you try to limit your time on the sidewalk, but other studies have shown that there is no difference in the amount of stress on your body when you run on the sidewalk versus running on the road. But if you have injuries like ankle sprains or knee pain, it's probably best if you stay off the road.");
        dm.setNote("If you want to run on concrete, make sure you have the best footwear for the job. Wear shoes with adequate cushioning if you find that running on concrete leads to joint pain.");
        dm.setImgSrc(R.drawable.concrete);
        objList.add(dm);

        dm = new DataModel();
        dm.setGroundType("Dirt");
        dm.setPros("Behind grass, dirt roads are also often rated as one of the best surfaces to run on. Dirt has just enough hardness and leeway to make for a prime running surface, especially if you suffer from shin splints, IT band syndrome, or other impact-related injuries.");
        dm.setCons("The unevenness of dirt trails can be bad for your ankles, so avoid dirt roads if you've had an ankle injury.");
        dm.setNote("Like grass, dirt trails can be uneven, so pay close attention to where you're stepping.");
        dm.setImgSrc(R.drawable.dirt);
        objList.add(dm);

        dm = new DataModel();
        dm.setGroundType("Track");
        dm.setPros("The spongy surface of a synthetic track strikes the right balance between soft and sturdy.");
        dm.setCons("Long runs in the oval may be boring. Those with calf sprains and issues with the IT band should also look out: running around the track could shorten the muscles of the calf and strain the IT band.");
        dm.setNote("If you have these issues, keep track of short runs and try to make things smoother as you go around the corners.");
        dm.setImgSrc(R.drawable.track);
        objList.add(dm);

        return objList;
    }
}
