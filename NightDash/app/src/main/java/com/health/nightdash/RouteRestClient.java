package com.health.nightdash;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class RouteRestClient {

    private static final String BASE_URL_CRASHES = "https://4teq9tk94m.execute-api.ap-southeast-2.amazonaws.com/GetCrashes/";
    private static final String BASE_URL_STREET_LIGHTS = "https://ontej5gv79.execute-api.ap-southeast-2.amazonaws.com/GetStreetLights/";

    public static String getCrashes(int radius, Double latitude, Double longitude) {
        final String methodPath = (radius + "/" +latitude + "/" + longitude);
        //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        //Making HTTP request
        try {
            url = new URL(BASE_URL_CRASHES + methodPath);
            //open the connection
            conn = (HttpURLConnection) url.openConnection(); //set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000); //set the connection method to GET
            conn.setRequestMethod("GET"); //add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return textResult;
    }

    public static String getStreetLights(int radius, Double latitude, Double longitude) {
        final String methodPath = (radius + "/" +latitude + "/" + longitude);
        //initialise
        URL url = null;
        HttpURLConnection conn = null;
        String textResult = "";
        //Making HTTP request
        try {
            url = new URL(BASE_URL_STREET_LIGHTS + methodPath);
            //open the connection
            conn = (HttpURLConnection) url.openConnection(); //set the timeout
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000); //set the connection method to GET
            conn.setRequestMethod("GET"); //add http headers to set your response type to json
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); //Read the response
            Scanner inStream = new Scanner(conn.getInputStream()); //read the input steream and store it as string
            while (inStream.hasNextLine()) {
                textResult += inStream.nextLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return textResult;
    }
}
