package com.health.nightdash;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {

        this.context = context;

    }

    public int[] slider_images = {
        R.drawable.project_logo,
        R.drawable.path,
        R.drawable.ground,
        R.drawable.manual,
        R.drawable.notification
    };

    public String[] slider_title = {
            "WELCOME TO NIGHT DASH",
            "ROUTE",
            "PARKS",
            "FUEL UP",
            "REMINDERS"
    };

    public String[] slider_desc = {
            "NightDash is an easy to use application, helps for night running, to overcome depression.",
            "Check out routes near Melbourne CBD, with crashes and crimes in your area.",
            "Far away from Melbourne CBD? No worries. \n\nCheck out parks with specific ground type near you.",
            "New to night running. \n\nNot sure what and when to eat. \n\n Find it here.",
            "Busy with your schedule? No worries.\n\nNightDash reminds you about your run.\n\nAre you ready to explore NightDash?"

    };

    @Override
    public int getCount() {
        return slider_title.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView sliderImageVIew = (ImageView) view.findViewById(R.id.sliderImage);
        TextView sliderTitle = (TextView) view.findViewById(R.id.sliderTitle);
        TextView sliderDescription = (TextView) view.findViewById(R.id.sliderDescription);

        sliderImageVIew.setImageResource(slider_images[position]);
        sliderTitle.setText(slider_title[position]);
        sliderDescription.setText(slider_desc[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout)object);
    }
}
