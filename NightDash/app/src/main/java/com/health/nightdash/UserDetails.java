package com.health.nightdash;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class UserDetails {
    @PrimaryKey(autoGenerate = true)
    private int userDetailsid;
    @ColumnInfo(name = "radius")
    private int radius;
    @ColumnInfo(name = "distance")
    private int distance;
    @ColumnInfo(name = "time")
    private String time;
    @ColumnInfo(name = "surface_type")
    private String surface_type;
    @ColumnInfo(name = "first_time")
    private Boolean first_time;

    public UserDetails(int radius, int distance, String time, String surface_type, Boolean first_time)
    {
        this.radius = radius;
        this.distance = distance;
        this.time = time;
        this.surface_type = surface_type;
        this.first_time = first_time;
    }

    public UserDetails() {
    }

    public int getUserDetailsid() {
        return userDetailsid;
    }

    public void setUserDetailsid(int userDetailsid) {
        this.userDetailsid = userDetailsid;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getSurface_type()
    {
        return surface_type;
    }

    public void setSurface_type(String surface_type)
    {
        this.surface_type = surface_type;
    }

    public Boolean getFirst_time() {
        return first_time;
    }

    public void setFirst_time(Boolean first_time) {
        this.first_time = first_time;
    }
}
