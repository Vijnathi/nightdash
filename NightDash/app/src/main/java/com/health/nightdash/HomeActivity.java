package com.health.nightdash;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;

public class HomeActivity extends AppCompatActivity implements PermissionsListener {

    private TextView mTextMessage;
    private ActionBar myActionBar;

    private PermissionsManager permissionsManager;

    private BottomNavigationView navigation;

//    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private SettingsClient mSettingsClient;
    private LocationSettingsRequest mLocationSettingsRequest;
    private static final int REQUEST_CHECK_SETTINGS = 214;
    private static final int REQUEST_ENABLE_GPS = 516;
//    private ShowcaseView showcaseView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//            int id = item.getItemId();
            Fragment nextFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    myActionBar.setTitle("Set Run");
                    nextFragment = new ProfileFragment();
                    break;
                case R.id.navigation_dashboard:
                    myActionBar.setTitle("Dashboard");
                    nextFragment = new DashboardFragment();
                    break;
                case R.id.navigation_route:
                    myActionBar.setTitle("Route");
                    nextFragment = new RouteFragment();
                    break;
                case R.id.navigation_parks:
                    myActionBar.setTitle("Parks");
                    nextFragment = new ParksFragment();
                    break;
                case R.id.navigation_fuelUp:
                    myActionBar.setTitle("Fuel Up");
                    nextFragment = new FuelupFragment();
                    break;
            }

            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, nextFragment).commit();

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        myActionBar = getSupportActionBar();
        hideStatusBar();
        disableDisplayHome();
        mTextMessage = (TextView) findViewById(R.id.message);
        if (PermissionsManager.areLocationPermissionsGranted(HomeActivity.this)) {
            System.out.println("Do nothing");

        } else {
            permissionsManager = new PermissionsManager(HomeActivity.this);
            permissionsManager.requestLocationPermissions(HomeActivity.this);
        }
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY));
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        mSettingsClient = LocationServices.getSettingsClient(HomeActivity.this);

        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                        //Success Perform Task Here
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                navigation = (BottomNavigationView) findViewById(R.id.navigation);

                                navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

                                FragmentManager fragmentManager = getFragmentManager();

                                SharedPreferences preferences = getSharedPreferences("user_details", MODE_PRIVATE);
                                String userDetail = preferences.getString("details", null);
                                if (userDetail == null) {
                                    navigation.setSelectedItemId(R.id.navigation_profile);
                                    fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment()).commit();
                                } else {
                                    navigation.setSelectedItemId(R.id.navigation_dashboard);
                                    fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment()).commit();
                                }


                                Boolean firstTime = preferences.getBoolean("first_time", false);
                                if (!firstTime) {
                                    setAlarm();
                                }


                            }
                        }, 1500);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.e("GPS","Unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e("GPS","Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                        }
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.e("GPS","checkLocationSettings -> onCanceled");
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    //Success Perform Task Here


                    navigation = (BottomNavigationView) findViewById(R.id.navigation);

                    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

                    FragmentManager fragmentManager = getFragmentManager();

                    SharedPreferences preferences = getSharedPreferences("user_details", MODE_PRIVATE);
                    String userDetail = preferences.getString("details", null);
                    if (userDetail == null) {
                        navigation.setSelectedItemId(R.id.navigation_profile);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment()).commit();
                    } else {
                        navigation.setSelectedItemId(R.id.navigation_dashboard);
                        fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment()).commit();
                    }


                    Boolean firstTime = preferences.getBoolean("first_time", false);
                    if (!firstTime) {
                        setAlarm();
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    Log.e("GPS","User denied to access location");
                    openGpsEnableSetting();
                    break;
            }
        } else if (requestCode == REQUEST_ENABLE_GPS) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!isGpsEnabled) {
                openGpsEnableSetting();
            } else {
                if (PermissionsManager.areLocationPermissionsGranted(HomeActivity.this)) {
                    System.out.println("Do nothing");

                } else {
                    permissionsManager = new PermissionsManager(HomeActivity.this);
                    permissionsManager.requestLocationPermissions(HomeActivity.this);
                }

                navigation = (BottomNavigationView) findViewById(R.id.navigation);

                navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

                FragmentManager fragmentManager = getFragmentManager();

                SharedPreferences preferences = getSharedPreferences("user_details", MODE_PRIVATE);
                String userDetail = preferences.getString("details", null);
                if (userDetail == null) {
                    navigation.setSelectedItemId(R.id.navigation_profile);
                    fragmentManager.beginTransaction().replace(R.id.content_frame, new ProfileFragment()).commit();
                } else {
                    navigation.setSelectedItemId(R.id.navigation_dashboard);
                    fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment()).commit();
                }


                Boolean firstTime = preferences.getBoolean("first_time", false);
                if (!firstTime) {
                    setAlarm();
                }
            }
        }
    }

    private void openGpsEnableSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, REQUEST_ENABLE_GPS);
    }

    public void setAlarm(){
        SharedPreferences preferences = getSharedPreferences("user_details", MODE_PRIVATE);
        String arrayDetails[] = (preferences.getString("details", null)).split(" ");
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date scheduledTime;
        try {
            scheduledTime = format.parse(arrayDetails[3]);
            startAlarm(true, true, scheduledTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    private void startAlarm(boolean isNotification, boolean isRepeat, Date scheduledRunTime) {
        Intent myIntent;
        PendingIntent pendingIntent;

        // SET TIME HERE
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(scheduledRunTime);
        calendar.add(Calendar.HOUR, -1);
//        calendar.set(Calendar.HOUR_OF_DAY, 18);
//        calendar.set(Calendar.MINUTE, 20);


        myIntent = new Intent(this,AlarmNotificationReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this,0,myIntent,0);

//        cancelAlarmIfExists(this,0,myIntent);

        AlarmManager manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        if(!isRepeat)
            manager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime()+3000,pendingIntent);
        else
            manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY,pendingIntent);
    }

    public void cancelAlarmIfExists(Context mContext,int requestCode,Intent intent){
        try {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, intent,0);
            AlarmManager am=(AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
            am.cancel(pendingIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void navigate(int item_id) {
        Fragment nextFragment = null;
        switch (item_id) {
            case R.id.navigation_profile:
                myActionBar.setTitle("Profile");
                nextFragment = new ProfileFragment();
                break;
            case R.id.navigation_dashboard:
                myActionBar.setTitle("Dashboard");
                nextFragment = new DashboardFragment();
                break;
            case R.id.navigation_route:
                myActionBar.setTitle("Route");
                nextFragment = new RouteFragment();
                break;
            case R.id.navigation_parks:
                myActionBar.setTitle("Parks");
                nextFragment = new ParksFragment();
                break;
            case R.id.navigation_fuelUp:
                myActionBar.setTitle("Fuel Up");
                nextFragment = new FuelupFragment();
                break;
        }

        if(item_id == 6) {
            navigation.setSelectedItemId(R.id.navigation_dashboard);
        } else {
            navigation.setSelectedItemId(item_id);
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, nextFragment).commit();
    }
    
    public void hideStatusBar() {
        myActionBar.hide();
    }
    public void showStatusBar() {
        myActionBar.show();
    }

    public void enableDisplayHome(){
        myActionBar.setDisplayHomeAsUpEnabled(true);
    }
    public void disableDisplayHome(){
        myActionBar.setDisplayHomeAsUpEnabled(false);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void enableBottomBar(boolean enable){
        for (int i = 0; i < navigation.getMenu().size(); i++) {
            navigation.getMenu().getItem(i).setEnabled(enable);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        System.out.println(granted);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Close App?")
                .setMessage("Do you really want to close the app?")
                .setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                            }
                        }).show();
    }
}
