package com.health.nightdash;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomCardAdapter extends RecyclerView.Adapter<CustomCardAdapter.MyViewHolder> {

    ArrayList<DataModel> listDataModel = new ArrayList<DataModel>();

    public CustomCardAdapter(ArrayList<DataModel> _listDataModel) {
        this.listDataModel = _listDataModel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ground_type_card_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        DataModel objDataModel = listDataModel.get(position);

        holder.imgGroundType.setImageResource(objDataModel.getImgSrc());
        holder.groundTypeTitleText.setText(objDataModel.getGroundType().toString());
        holder.textViewPros.setText(objDataModel.getPros().toString());
        holder.textViewCons.setText(objDataModel.getCons().toString());
        holder.textViewNote.setText(objDataModel.getNote().toString());
    }

    @Override
    public int getItemCount() {
        return listDataModel.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView groundTypeTitleText, textViewPros, textViewCons, textViewNote;
        ImageView imgGroundType;

        public MyViewHolder(View view) {
            super(view);
            imgGroundType = (ImageView) view.findViewById(R.id.imgGroundType);
            groundTypeTitleText = (TextView) view.findViewById(R.id.groundTypeCardTitle);
            textViewPros = (TextView) view.findViewById(R.id.textViewPros);
            textViewCons = (TextView) view.findViewById(R.id.textViewCons);
            textViewNote = (TextView) view.findViewById(R.id.textViewNote);
        }
    }
}
