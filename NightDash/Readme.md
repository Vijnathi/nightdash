﻿# Android App

Assists night runners.

## Main Features

* Parks
* Routes
* Fuel Up
* Ground Type Manual
* Get notified about Run

## Contributor

**Team E14 - Divide And Conquer**

**Topic** - Promoting health through physical activities and sports
**Project** - NightDash

**Team Members -** 
* **Vijnathi Katamaneni** - Master of Information Technology - Application Developer
* **Vivardhan Ramesh** - Master of Information Technology - Application Developer
