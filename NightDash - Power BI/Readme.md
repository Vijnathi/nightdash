﻿# Parks Data Visualisation by Power BI

To open or run this file you need to [install PowerBI Desktop][] or use [online][]. If you don't have account, you need to signup. 

[install PowerBI Desktop]: https://powerbi.microsoft.com/en-us/desktop/
[online]: https://powerbi.microsoft.com/en-us/

## Contributor

**Team E14 - Divide And Conquer**

**Topic** - Promoting health through physical activities and sports
**Project** - NightDash

**Team Member -** 
* **Yiyuan Jiang (Barton)** - Master of Data Science - Data Analyst
